package dk.mehmedbasic.yatseunlocker;

import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.*;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;

import java.util.List;

/**
 * A wrapping package manager that fools the yatse application into thinking we bought it.
 */
public class OwningYatsePackageManager extends PackageManager {
    private PackageManager wrapped;

    public OwningYatsePackageManager(PackageManager wrapped) {
        this.wrapped = wrapped;
    }

    @Override
    public PackageInfo getPackageInfo(String packageName, int flags) throws NameNotFoundException {
        if (packageName.equals("org.leetzone.android.yatsewidgetunlocker")) {
            return new OwningYatsePackageInfo();
        }

        return wrapped.getPackageInfo(packageName, flags);
    }

    @Override
    public String[] currentToCanonicalPackageNames(String[] strings) {
        return wrapped.currentToCanonicalPackageNames(strings);
    }

    @Override
    public String[] canonicalToCurrentPackageNames(String[] strings) {
        return wrapped.canonicalToCurrentPackageNames(strings);
    }

    @Override
    public Intent getLaunchIntentForPackage(String s) {
        return wrapped.getLaunchIntentForPackage(s);
    }

    @Override
    public int[] getPackageGids(String s) throws NameNotFoundException {
        return wrapped.getPackageGids(s);
    }

    @Override
    public PermissionInfo getPermissionInfo(String s, int i) throws NameNotFoundException {
        return wrapped.getPermissionInfo(s, i);
    }

    @Override
    public List<PermissionInfo> queryPermissionsByGroup(String s, int i) throws NameNotFoundException {
        return wrapped.queryPermissionsByGroup(s, i);
    }

    @Override
    public PermissionGroupInfo getPermissionGroupInfo(String s, int i) throws NameNotFoundException {
        return wrapped.getPermissionGroupInfo(s, i);
    }

    @Override
    public List<PermissionGroupInfo> getAllPermissionGroups(int i) {
        return wrapped.getAllPermissionGroups(i);
    }

    @Override
    public ApplicationInfo getApplicationInfo(String s, int i) throws NameNotFoundException {
        return wrapped.getApplicationInfo(s, i);
    }

    @Override
    public ActivityInfo getActivityInfo(ComponentName componentName, int i) throws NameNotFoundException {
        return wrapped.getActivityInfo(componentName, i);
    }

    @Override
    public ActivityInfo getReceiverInfo(ComponentName componentName, int i) throws NameNotFoundException {
        return wrapped.getReceiverInfo(componentName, i);
    }

    @Override
    public ServiceInfo getServiceInfo(ComponentName componentName, int i) throws NameNotFoundException {
        return wrapped.getServiceInfo(componentName, i);
    }

    @Override
    public ProviderInfo getProviderInfo(ComponentName componentName, int i) throws NameNotFoundException {
        return wrapped.getProviderInfo(componentName, i);
    }

    @Override
    public List<PackageInfo> getInstalledPackages(int i) {
        return wrapped.getInstalledPackages(i);
    }

    @Override
    public int checkPermission(String s, String s1) {
        return wrapped.checkPermission(s, s1);
    }

    @Override
    public boolean addPermission(PermissionInfo permissionInfo) {
        return wrapped.addPermission(permissionInfo);
    }

    @Override
    public boolean addPermissionAsync(PermissionInfo permissionInfo) {
        return wrapped.addPermissionAsync(permissionInfo);
    }

    @Override
    public void removePermission(String s) {
        wrapped.removePermission(s);
    }

    @Override
    public int checkSignatures(String s, String s1) {
        return 0;
    }

    @Override
    public int checkSignatures(int i, int i1) {
        return 0;
    }

    @Override
    public String[] getPackagesForUid(int i) {
        return wrapped.getPackagesForUid(i);
    }

    @Override
    public String getNameForUid(int i) {
        return wrapped.getNameForUid(i);
    }

    @Override
    public List<ApplicationInfo> getInstalledApplications(int i) {
        return wrapped.getInstalledApplications(i);
    }

    @Override
    public String[] getSystemSharedLibraryNames() {
        return wrapped.getSystemSharedLibraryNames();
    }

    @Override
    public FeatureInfo[] getSystemAvailableFeatures() {
        return wrapped.getSystemAvailableFeatures();
    }

    @Override
    public boolean hasSystemFeature(String s) {
        return wrapped.hasSystemFeature(s);
    }

    @Override
    public ResolveInfo resolveActivity(Intent intent, int i) {
        return wrapped.resolveActivity(intent, i);
    }

    @Override
    public List<ResolveInfo> queryIntentActivities(Intent intent, int i) {
        return wrapped.queryIntentActivities(intent, i);
    }

    @Override
    public List<ResolveInfo> queryIntentActivityOptions(ComponentName componentName, Intent[] intents, Intent intent, int i) {
        return wrapped.queryIntentActivityOptions(componentName, intents, intent, i);
    }

    @Override
    public List<ResolveInfo> queryBroadcastReceivers(Intent intent, int i) {
        return wrapped.queryBroadcastReceivers(intent, i);
    }

    @Override
    public ResolveInfo resolveService(Intent intent, int i) {
        return wrapped.resolveService(intent, i);
    }

    @Override
    public List<ResolveInfo> queryIntentServices(Intent intent, int i) {
        return wrapped.queryIntentServices(intent, i);
    }

    @Override
    public ProviderInfo resolveContentProvider(String s, int i) {
        return wrapped.resolveContentProvider(s, i);
    }

    @Override
    public List<ProviderInfo> queryContentProviders(String s, int i, int i1) {
        return wrapped.queryContentProviders(s, i, i1);
    }

    @Override
    public InstrumentationInfo getInstrumentationInfo(ComponentName componentName, int i) throws NameNotFoundException {
        return wrapped.getInstrumentationInfo(componentName, i);
    }

    @Override
    public List<InstrumentationInfo> queryInstrumentation(String s, int i) {
        return wrapped.queryInstrumentation(s, i);
    }

    @Override
    public Drawable getDrawable(String s, int i, ApplicationInfo applicationInfo) {
        return wrapped.getDrawable(s, i, applicationInfo);
    }

    @Override
    public Drawable getActivityIcon(ComponentName componentName) throws NameNotFoundException {
        return wrapped.getActivityIcon(componentName);
    }

    @Override
    public Drawable getActivityIcon(Intent intent) throws NameNotFoundException {
        return wrapped.getActivityIcon(intent);
    }

    @Override
    public Drawable getDefaultActivityIcon() {
        return wrapped.getDefaultActivityIcon();
    }

    @Override
    public Drawable getApplicationIcon(ApplicationInfo applicationInfo) {
        return wrapped.getApplicationIcon(applicationInfo);
    }

    @Override
    public Drawable getApplicationIcon(String s) throws NameNotFoundException {
        return wrapped.getApplicationIcon(s);
    }

    @Override
    public Drawable getActivityLogo(ComponentName componentName) throws NameNotFoundException {
        return wrapped.getActivityLogo(componentName);
    }

    @Override
    public Drawable getActivityLogo(Intent intent) throws NameNotFoundException {
        return wrapped.getActivityLogo(intent);
    }

    @Override
    public Drawable getApplicationLogo(ApplicationInfo applicationInfo) {
        return wrapped.getApplicationLogo(applicationInfo);
    }

    @Override
    public Drawable getApplicationLogo(String s) throws NameNotFoundException {
        return wrapped.getApplicationLogo(s);
    }

    @Override
    public CharSequence getText(String s, int i, ApplicationInfo applicationInfo) {
        return wrapped.getText(s, i, applicationInfo);
    }

    @Override
    public XmlResourceParser getXml(String s, int i, ApplicationInfo applicationInfo) {
        return wrapped.getXml(s, i, applicationInfo);
    }

    @Override
    public CharSequence getApplicationLabel(ApplicationInfo applicationInfo) {
        return wrapped.getApplicationLabel(applicationInfo);
    }

    @Override
    public Resources getResourcesForActivity(ComponentName componentName) throws NameNotFoundException {
        return wrapped.getResourcesForActivity(componentName);
    }

    @Override
    public Resources getResourcesForApplication(ApplicationInfo applicationInfo) throws NameNotFoundException {
        return wrapped.getResourcesForApplication(applicationInfo);
    }

    @Override
    public Resources getResourcesForApplication(String s) throws NameNotFoundException {
        return wrapped.getResourcesForApplication(s);
    }

    @Override
    public PackageInfo getPackageArchiveInfo(String archiveFilePath, int flags) {
        return wrapped.getPackageArchiveInfo(archiveFilePath, flags);
    }

    @Override
    public void verifyPendingInstall(int i, int i1) {
        wrapped.verifyPendingInstall(i, i1);
    }

    @Override
    public void setInstallerPackageName(String s, String s1) {
        wrapped.setInstallerPackageName(s, s1);
    }

    @Override
    public String getInstallerPackageName(String s) {
        return wrapped.getInstallerPackageName(s);
    }

    @Override
    @Deprecated
    public void addPackageToPreferred(String s) {
        wrapped.addPackageToPreferred(s);
    }

    @Override
    @Deprecated
    public void removePackageFromPreferred(String s) {
        wrapped.removePackageFromPreferred(s);
    }

    @Override
    public List<PackageInfo> getPreferredPackages(int i) {
        return wrapped.getPreferredPackages(i);
    }

    @Override
    @Deprecated
    public void addPreferredActivity(IntentFilter intentFilter, int i, ComponentName[] componentNames, ComponentName componentName) {
        wrapped.addPreferredActivity(intentFilter, i, componentNames, componentName);
    }

    @Override
    public void clearPackagePreferredActivities(String s) {
        wrapped.clearPackagePreferredActivities(s);
    }

    @Override
    public int getPreferredActivities(List<IntentFilter> list, List<ComponentName> list1, String s) {
        return wrapped.getPreferredActivities(list, list1, s);
    }

    @Override
    public void setComponentEnabledSetting(ComponentName componentName, int i, int i1) {
        wrapped.setComponentEnabledSetting(componentName, i, i1);
    }

    @Override
    public int getComponentEnabledSetting(ComponentName componentName) {
        return wrapped.getComponentEnabledSetting(componentName);
    }

    @Override
    public void setApplicationEnabledSetting(String s, int i, int i1) {
        wrapped.setApplicationEnabledSetting(s, i, i1);
    }

    @Override
    public int getApplicationEnabledSetting(String s) {
        return wrapped.getApplicationEnabledSetting(s);
    }

    @Override
    public boolean isSafeMode() {
        return wrapped.isSafeMode();
    }
}
