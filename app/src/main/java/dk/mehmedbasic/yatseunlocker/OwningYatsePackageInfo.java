package dk.mehmedbasic.yatseunlocker;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Bundle;

/**
 * A package info to fool the yatse unlock checker.
 */
class OwningYatsePackageInfo extends PackageInfo {
    OwningYatsePackageInfo() {
        packageName = "org.leetzone.android.yatsewidgetunlocker";
        versionName = "2.1.0";
        ApplicationInfo info = new ApplicationInfo();
        info.metaData = new Bundle();
        this.applicationInfo = info;
    }
}
