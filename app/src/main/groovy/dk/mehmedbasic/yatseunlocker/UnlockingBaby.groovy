package dk.mehmedbasic.yatseunlocker

import android.content.pm.PackageManager
import android.util.Log
import de.robv.android.xposed.XC_MethodHook
import de.robv.android.xposed.XposedHelpers
import dk.mehmedbasic.groovyxposedbridge.GroovyXposed

import java.lang.ref.WeakReference

/**
 * This class hooks the methods we need.
 */
class UnlockingBaby extends GroovyXposed {
    public UnlockingBaby() {
        super("org.leetzone.android.yatsewidgetfree")
    }

    @Override
    void handleLoadedPackage() {
        def applicationClass = findClass("org.leetzone.android.yatsewidget.YatseApplication")
        def contextClass = findClass("android.content.ContextWrapper")

        XposedHelpers.findAndHookMethod(contextClass, "getPackageManager", new XC_MethodHook() {
            WeakReference<PackageManager> reference

            @Override
            protected void afterHookedMethod(XC_MethodHook.MethodHookParam param) throws Throwable {
                if (param.thisObject.getClass() == applicationClass) {
                    if (reference == null || reference.get() == null) {
                        Log.d("YatseUnlocker", "Returning pure pwnage.")
                        def wrapped = param.result as PackageManager
                        reference = new WeakReference<>(new OwningYatsePackageManager(wrapped))
                    }
                    param.result = reference.get()
                }
            }
        })


    }
}
