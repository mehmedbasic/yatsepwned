# README #

This application is a demonstration of how Xposed can be used to break anti-piracy measures in an app.

### What does it do? ###

It unlocks the Yatse Kodi remote application by making the app think the unlocker is installed.

### How does it do it? ###

Using the Xposed framework the few lines of code intercept calls to `getPackageManager()` and return a malicious implementation of it.

This new implementation returns true on all `checkSignature` calls (something LuckyPatcher also can do). It also returns a valid package information object for when the application asks for the real, paid for unlocker. 

### How can the developer fix it? ###

He can't without breaking legacy unlockers.

The only way to protect yourself from this type of hack is to make the application cost money instead of having unlockers.

### Techical stuff ###

Xposed hooks into the `android.content.ContextWrapper#getPackageManager()` method and returns a decorating package manager.

The package manager lets the magic happen in the following method:

```java
@Override
public PackageInfo getPackageInfo(String packageName, int flags) throws NameNotFoundException {
    if (packageName.equals("org.leetzone.android.yatsewidgetunlocker")) {
        return new OwningYatsePackageInfo();
    }

    return wrapped.getPackageInfo(packageName, flags);
}
```

OwningPackageInfo is a very small class:
```java
class OwningYatsePackageInfo extends PackageInfo {
    OwningYatsePackageInfo() {
        packageName = "org.leetzone.android.yatsewidgetunlocker";
        versionName = "2.1.0";
        ApplicationInfo info = new ApplicationInfo();
        info.metaData = new Bundle();
        this.applicationInfo = info;
    }
}
```

This shows the simplicity of breaking applications when you have root access.
With very few lines of code, the Yatse developers hard work is free.

### Don't be an ass just because you can ###

Don't be an ass and buy the stuff to support the developers.
